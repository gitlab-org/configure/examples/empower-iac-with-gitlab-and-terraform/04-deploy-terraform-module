# 04 - Deploy Terraform Module

This project illustrates a basic setup to deploy a Terraform Module
to the [GitLab Terraform Module Registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/index.html).

It uses the Terraform Module CI/CD template which is already released on GitLab.com and will be available with 15.9 on self-managed
instances.

See the deployed Terraform Module releases [here](https://gitlab.com/gitlab-org/configure/examples/empower-iac-with-gitlab-and-terraform/04-deploy-terraform-module/-/infrastructure_registry).